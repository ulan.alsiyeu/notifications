package domain

type NotificationDTO struct {
	UserID  int
	Message string
}
