package domain

const (
	SentStatus      = "sent"
	DeliveredStatus = "delivered"
	ReadStatus      = "read"
	FailedStatus    = "failed"
)

func NewNotification(user *User, message string) *Notification {
	return &Notification{
		ID:      2,
		User:    user,
		Message: message,
		amount:  1,
	}
}

// Notification
// 1. Структура является представлением принципа ООП - Абстракция. Внешнему коду видна сама структура и метод, но реализиця логики amount скрыта
// 2. Поле amount является представлением принципа ООП - Инкапсуляция. Значение поля скрыто от внешнего кода
type Notification struct {
	ID      int
	User    *User
	Message string
	amount  int
}

func (n *Notification) GetNotificationsAmount() int {
	return n.amount
}

// User
// 1. Структура является представлением принципа SOLID - SRP. Объект имеет только одну отсетственность над сущностью User,
// изменения любых других объектов на него не влияют
type User struct {
	ID          int
	Name        string
	PhoneNumber string
	Email       string
}

func (u *User) GetUserName() string {
	return u.Name
}

// WhatsappUser
// 1. Структура реализует принцип ООП - Наследование. Структура через композицию содержит поля User и имеет к ним прямой доступ
type WhatsappUser struct {
	User
	WhatsAppID string
}

type TelegramUser struct {
	User
	Username string
}

type NotificationStatistics struct {
	SentAmount      int
	DeliveredAmount int
	ReadAmount      int
	DeletedAmount   int
	FailedAmount    int
}
