package domain

import "context"

// 1. Оба интерфейса реализуют принцип SOLID - ISP. Под разные задачи используются разные интерфейсы.
// Если новому use case-у нужен будет только UserRepo то он не зависит от изменений в NotificationRepo

type UserRepo interface {
	GetByID(ctx context.Context, userID int) (*User, error)
}

type NotificationRepo interface {
	Create(ctx context.Context, notification *Notification) error
}
