package domain

import "context"

type SendNotification interface {
	Handle(ctx context.Context, dto NotificationDTO) error
}

type GetNotificationsStatistics interface {
	Handle(ctx context.Context) (*NotificationStatistics, error)
}
