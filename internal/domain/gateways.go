package domain

import "context"

// NotificationSender
// 1. Интерфейс реализует принцип ООП - Полморфизм. Разные объекты whatsapp, telegram и email могут иметь разные реализации метода Send
// 2. Связанный с интерфейсом код реализует принцип SOLID - OCP. Клиентский код в use case зависит от интерфейса.
// Если нужно расширить функционал то, создается новая реализация, а старая не меняется
// 3. По сути использование интерфейса дает возможность реализовать принцип SOLID - LSP. Разные реализации интерфейса без проблем могут быть взаимозаменяемы
type NotificationSender interface {
	Send(ctx context.Context, notification *Notification) error
}

// NotificationStatisticsCollector
// Клиентский интерфейс для адаптера
type NotificationStatisticsCollector interface {
	Collect(ctx context.Context) (*NotificationStatistics, error)
}
