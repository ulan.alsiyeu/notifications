package interactors

import (
	"context"
	"example.com/m/v2/internal/domain"
)

func NewGetNotificationStatistics(collector domain.NotificationStatisticsCollector) *GetNotificationStatistics {
	return &GetNotificationStatistics{collector: collector}
}

type GetNotificationStatistics struct {
	collector domain.NotificationStatisticsCollector
}

func (gns *GetNotificationStatistics) Handle(ctx context.Context) (*domain.NotificationStatistics, error) {
	return gns.collector.Collect(ctx)
}
