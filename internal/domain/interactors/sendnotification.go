package interactors

import (
	"context"
	"example.com/m/v2/internal/domain"
	"example.com/m/v2/internal/infrastructure"
)

func NewSendNotification(
	usersRepo domain.UserRepo,
	notificationRepo domain.NotificationRepo,
	notificationGateway domain.NotificationSender,
) *SendNotification {
	return &SendNotification{
		UsersRepo:           usersRepo,
		NotificationRepo:    notificationRepo,
		NotificationGateway: notificationGateway,
	}
}

type SendNotification struct {
	infrastructure.Publisher
	UsersRepo           domain.UserRepo
	NotificationRepo    domain.NotificationRepo
	NotificationGateway domain.NotificationSender
}

func (sn *SendNotification) Handle(ctx context.Context, dto domain.NotificationDTO) error {
	user, err := sn.UsersRepo.GetByID(ctx, dto.UserID)
	if err != nil {
		return err
	}

	notification := domain.NewNotification(user, dto.Message)

	err = sn.NotificationRepo.Create(ctx, notification)
	if err != nil {
		return err
	}

	sn.NotifyAll(notification.Message)

	// 1. Здесь клиентский код вообще не зависит от конкретных объектов, реализую тем самым принцип SOLID - DIP
	return sn.NotificationGateway.Send(ctx, notification)
}
