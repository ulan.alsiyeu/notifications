package adapters

import (
	"context"
	"encoding/json"
	"example.com/m/v2/internal/domain"
	"example.com/m/v2/internal/infrastructure/providers"
)

// StatisticsProxy
// Прокси который кэширует
type StatisticsProxy struct {
	cache      providers.Cache
	statistics StatisticsAdapter
}

func (sp *StatisticsProxy) Collect(ctx context.Context) (*domain.NotificationStatistics, error) {
	var statistics *domain.NotificationStatistics
	var err error

	cached := sp.cache.Get()
	err = json.Unmarshal(cached, &statistics)
	if err != nil {
		return nil, err
	}

	if statistics == nil {
		statistics, err = sp.statistics.Collect(ctx)
		if err != nil {
			return nil, err
		}

		marshalled, err := json.Marshal(statistics)
		if err != nil {
			return nil, err
		}

		err = sp.cache.Set("newStat", marshalled)
		if err != nil {
			return nil, err
		}
	}

	return statistics, err
}
