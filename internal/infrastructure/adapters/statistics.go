package adapters

import (
	"context"
	"example.com/m/v2/internal/domain"
	"example.com/m/v2/internal/infrastructure/gateways/email"
	"example.com/m/v2/internal/infrastructure/gateways/telegram"
	"example.com/m/v2/internal/infrastructure/gateways/whatsapp"
)

func NewStatisticsAdapter(
	whatsAppGateway whatsapp.Gateway,
	telegramGateway telegram.Gateway,
	emailGateway email.Gateway,
) *StatisticsAdapter {
	return &StatisticsAdapter{
		whatsAppGateway: whatsAppGateway,
		telegramGateway: telegramGateway,
		emailGateway:    emailGateway,
	}
}

// StatisticsAdapter
// Сам адаптер
type StatisticsAdapter struct {
	whatsAppGateway whatsapp.Gateway
	telegramGateway telegram.Gateway
	emailGateway    email.Gateway
}

func (sa *StatisticsAdapter) Collect(ctx context.Context) (*domain.NotificationStatistics, error) {
	statistics := &domain.NotificationStatistics{}

	waStatuses, err := sa.whatsAppGateway.GetStatuses(ctx)
	if err != nil {
		return nil, err
	}

	for _, was := range waStatuses.Messages {
		switch was.Status {
		case domain.SentStatus:
			statistics.SentAmount++
		case domain.DeliveredStatus:
			statistics.DeliveredAmount++
		case domain.ReadStatus:
			statistics.ReadAmount++
		case domain.FailedStatus:
			statistics.FailedAmount++
		}
	}

	tgStat, err := sa.telegramGateway.GetStatistics(ctx)
	if err != nil {
		return nil, err
	}

	statistics.ReadAmount = statistics.ReadAmount + tgStat.Read
	statistics.DeliveredAmount = statistics.DeliveredAmount + tgStat.Delivered

	emailStat, err := sa.emailGateway.CollectEmailsInfo(ctx)
	if err != nil {
		return nil, err
	}

	for status, amount := range emailStat {
		switch status {
		case domain.SentStatus:
			statistics.SentAmount = statistics.SentAmount + amount
		case domain.DeliveredStatus:
			statistics.DeliveredAmount = statistics.DeliveredAmount + amount
		case domain.ReadStatus:
			statistics.ReadAmount = statistics.ReadAmount + amount
		case domain.FailedStatus:
			statistics.FailedAmount = statistics.FailedAmount + amount
		}
	}

	return statistics, nil
}
