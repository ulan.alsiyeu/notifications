package infrastructure

type Publisher interface {
	Subscribe(subscriber Subscriber)
	Unsubscribe(subscriber Subscriber)
	NotifyAll(message string)
}

type BasicPublisher struct {
	subscribers []Subscriber
}

func (p *BasicPublisher) Subscribe(subscriber Subscriber) {
	p.subscribers = append(p.subscribers, subscriber)
}

func (p *BasicPublisher) Unsubscribe(subscriber Subscriber) {
	amount := len(p.subscribers)

	for i, s := range p.subscribers {
		if subscriber.GetKey() == s.GetKey() {
			p.subscribers[amount-1], p.subscribers[i] = p.subscribers[i], p.subscribers[amount-1]
			p.subscribers = p.subscribers[:amount-1]
			return
		}
	}
}

func (p *BasicPublisher) NotifyAll(message string) {
	for _, s := range p.subscribers {
		s.Notify(message)
	}
}

type Subscriber interface {
	Notify(message string)
	GetKey() string
}
