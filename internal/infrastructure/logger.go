package infrastructure

import "github.com/rs/zerolog/log"

type Logger struct {
	key string
}

func (l *Logger) Notify(message string) {
	log.Info().Msg(message)
}

func (l *Logger) GetKey() string {
	return l.key
}
