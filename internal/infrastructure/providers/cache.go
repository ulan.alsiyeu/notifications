package providers

type Cache struct{}

func (c *Cache) Get() []byte {
	return nil
}

func (c *Cache) Set(key string, value []byte) error {
	return nil
}
