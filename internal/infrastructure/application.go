package infrastructure

import (
	"context"
	"example.com/m/v2/internal/domain"
	"log"
)

func NewApplication(mainUseCase domain.SendNotification) (*Application, error) {
	return &Application{mainUseCase: mainUseCase}, nil
}

type Application struct {
	mainUseCase domain.SendNotification
}

func (a *Application) Run(ctx context.Context, dto domain.NotificationDTO) {
	if err := a.mainUseCase.Handle(ctx, dto); err != nil {
		log.Fatal("main use case failed")
	}
}
