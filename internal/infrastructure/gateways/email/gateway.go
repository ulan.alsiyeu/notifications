package email

import (
	"context"
	"example.com/m/v2/internal/domain"
	"gitlab.com/ulan.alsiyeu/email"
)

func NewGateway() *Gateway {
	return &Gateway{}
}

// Gateway
// Здесь теперь используется фасад email
type Gateway struct{}

func (g *Gateway) Send(ctx context.Context, notification *domain.Notification) error {
	mail := &email.Email{
		ID:      notification.ID,
		Address: notification.User.Email,
		Message: notification.Message,
	}

	service := email.EmailService{}

	return service.Send(ctx, mail)
}

func (g *Gateway) CollectEmailsInfo(ctx context.Context) (map[string]int, error) {
	service := email.EmailService{}

	return service.CollectEmailsInfo(ctx)
}
