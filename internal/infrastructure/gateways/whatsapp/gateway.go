package whatsapp

import (
	"context"
	"example.com/m/v2/internal/domain"
	"fmt"
)

func NewGateway() *Gateway {
	return &Gateway{}
}

type Gateway struct{}

func (g *Gateway) Send(ctx context.Context, notification *domain.Notification) error {
	fmt.Println(fmt.Sprintf("Notification with id = %d is sent to whatsapp", notification.ID))
	return nil
}

func (g *Gateway) GetStatuses(ctx context.Context) (*StatusesResponse, error) {
	return &StatusesResponse{Messages: []*Message{{
		ID:     1,
		Status: domain.DeliveredStatus,
	}}}, nil
}
