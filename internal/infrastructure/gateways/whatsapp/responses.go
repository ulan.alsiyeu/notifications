package whatsapp

type StatusesResponse struct {
	Messages []*Message
}

type Message struct {
	ID     int
	Status string
}
