package telegram

type StatisticsResponse struct {
	Delivered int
	Read      int
}
