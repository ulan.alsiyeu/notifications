package telegram

import (
	"context"
	"example.com/m/v2/internal/domain"
	"fmt"
)

func NewGateway() *Gateway {
	return &Gateway{}
}

type Gateway struct{}

func (g *Gateway) Send(ctx context.Context, notification *domain.Notification) error {
	fmt.Println(fmt.Sprintf("Notification with id = %d is sent to telegram", notification.ID))
	return nil
}

func (g *Gateway) GetStatistics(ctx context.Context) (*StatisticsResponse, error) {
	return &StatisticsResponse{
		Delivered: 50,
		Read:      68,
	}, nil
}
