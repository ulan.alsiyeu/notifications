package notification

import (
	"context"
	"example.com/m/v2/internal/domain"
	"fmt"
)

func NewRepository() *Repository {
	return &Repository{}
}

type Repository struct{}

func (r *Repository) Create(ctx context.Context, notification *domain.Notification) error {
	fmt.Println("Notification created in db")
	return nil
}
