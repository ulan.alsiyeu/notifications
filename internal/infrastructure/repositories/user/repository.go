package user

import (
	"context"
	"example.com/m/v2/internal/domain"
)

func NewRepository() *Repository {
	return &Repository{}
}

type Repository struct{}

func (r *Repository) GetByID(ctx context.Context, userID int) (*domain.User, error) {
	return &domain.User{
		ID:          userID,
		Name:        "Ulan",
		PhoneNumber: "77059020198",
		Email:       "ulan.alsiyeu@gmail.com",
	}, nil
}
