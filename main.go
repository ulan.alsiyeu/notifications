package main

import (
	"context"
	"example.com/m/v2/internal/domain"
	"example.com/m/v2/internal/domain/interactors"
	"example.com/m/v2/internal/infrastructure"
	"example.com/m/v2/internal/infrastructure/gateways/telegram"
	"example.com/m/v2/internal/infrastructure/repositories/notification"
	"example.com/m/v2/internal/infrastructure/repositories/user"
	"log"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger := infrastructure.Logger{}

	userRepo := user.NewRepository()
	notificationRepo := notification.NewRepository()
	notificationGateway := telegram.NewGateway()
	mainUseCase := interactors.NewSendNotification(userRepo, notificationRepo, notificationGateway)

	mainUseCase.Subscribe(&logger)

	application, err := infrastructure.NewApplication(mainUseCase)
	if err != nil {
		log.Fatal(err)
	}

	dto := domain.NotificationDTO{
		UserID:  1,
		Message: "Hello world",
	}

	application.Run(ctx, dto)
}
