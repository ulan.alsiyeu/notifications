module example.com/m/v2

go 1.21.1

require (
	github.com/rs/zerolog v1.31.0
	gitlab.com/ulan.alsiyeu/email v1.0.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
